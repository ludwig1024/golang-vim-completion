#!/bin/bash -x
SRC=~/dev/gocode-autocompl/vim
cp $SRC/autoload/gocomplete.vim ./autoload/
cp $SRC/ftplugin/go/gocomplete.vim ./ftplugin/go/
